<?php


use Phinx\Migration\AbstractMigration;

class CreateUsersTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table("users")
            ->addColumn("email", "string")
            ->addColumn("password", "string")
            ->addColumn("active", "boolean", ["default" => false])
            ->addColumn("active_hash", "string", ["null" => true, "default" => null])
            ->addColumn("recover_hash", "string", ["null" => true, "default" => null])
            ->addColumn("remember_identifier", "string", ["null" => true, "default" => null])
            ->addColumn("remember_token", "string", ["null" => true, "default" => null])
            ->addTimestamps()
            ->create();
    }
}
