<?php

use Slim\Slim;
use Slim\Views\Twig;
use Slim\Views\TwigExtension;

use Noodlehaus\Config;
use RandomLib\Factory as RandomLib;
use Respect\Validation\Validator as v;
use PHPMailer\PHPMailer\PHPMailer;

use App\Models\User;

use App\Helpers\Hash;

use App\Mail\Mailer;

use App\Validation\Validator;

use App\Middleware\BeforeMiddleware;
use App\Middleware\CsrfMiddleware;

define("INC_ROOT", dirname(__DIR__));

require_once INC_ROOT . "/vendor/autoload.php";

$app = new Slim([
    "mode" => trim(file_get_contents(INC_ROOT . "/mode.php")),
    "view" => new Twig(),
    "templates.path" => INC_ROOT . "/resources/views"
]);

$app->add(new BeforeMiddleware);
$app->add(new CsrfMiddleware);

// Import config file into Slim Container
$app->configureMode($app->config("mode"), function () use ($app) {
    $app->config = Config::load(INC_ROOT . "/config/{$app->mode}.php");
});

// Set display errors to true for development environment (Setting in Config File)
if ($app->config->get("app.debug") === true) {
    ini_set("display_errors", "on");
}

require_once INC_ROOT . "/app/database.php";
require_once INC_ROOT . "/app/filters.php";
require_once INC_ROOT . "/app/routes.php";

$app->auth = false;

// Add User Model to Slim Container
$app->container->set("user", function () {
    return new User;
});

// Add Password Hashing Helper to Slim Container
$app->container->singleton("hash", function () use ($app) {
    return new Hash($app->config);
});

// Add Respect Validation via custom Validator to Slim Container
v::with("App\\Validation\\Rules\\");
$app->container->singleton("validation", function () {
    return new Validator();
});

// Add randomlib to Slim Container
$app->container->singleton("randomlib", function () {
    $factory = new RandomLib;
    return $factory->getMediumStrengthGenerator();
});

$app->container->singleton("mail", function () use ($app) {
    $mailer = new PHPMailer;

    $mailer->isSMTP();
    $mailer->Host = $app->config->get("mail.host");
    $mailer->SMTPAuth = $app->config->get("mail.smtp_auth");
    $mailer->SMTPSecure = $app->config->get("mail.smtp_secure");
    $mailer->Port = $app->config->get("mail.port");
    $mailer->Username = $app->config->get("mail.username");
    $mailer->Password = $app->config->get("mail.password");
    $mailer->setFrom($app->config->get("mail.username"), $app->config->get("mail.name"));

    $mailer->isHTML($app->config->get("mail.html"));

    return new Mailer($app->view, $mailer);
});

$view = $app->view();
$view->parserOptions = [
    'debug' => $app->config->get("app.debug"),
    // 'cache' => INC_ROOT . '/storage/cache'
];

$view->parserExtensions = [ new TwigExtension() ];
