<?php

define("ROUTES", INC_ROOT . "/app/routes");

// Authentication Routes
require_once ROUTES . "/auth/signin.php";
require_once ROUTES . "/auth/signup.php";
require_once ROUTES . "/auth/signout.php";
require_once ROUTES . "/auth/activate.php";

// General Routes
require_once ROUTES . "/home.php";
