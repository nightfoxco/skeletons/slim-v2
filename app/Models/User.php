<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class User extends Eloquent
{
    protected $table = "users";
    protected $fillable = [
        "email",
        "password",
        "active",
        "active_hash",
        "recover_hash",
        "remember_identifier",
        "remember_token"
    ];

    public function activateAccount()
    {
        $this->update([
            "active" => true,
            "active_hash" => null
        ]);
    }

    public function updateRememberCredentials($identifier, $token)
    {
        $this->update([
            "remember_identifier" => $identifier,
            "remember_token" => $token
        ]);
    }

    public function removeRememberCredentials()
    {
        $this->updateRememberCredentials(null, null);
    }
}
