<?php

// Function to check if user is logged in or not
$authenticationCheck = function ($required) use ($app) {
    return function () use ($required, $app) {
        if ((!$app->auth && $required) || ($app->auth && !$required)) {
            $app->redirect($app->urlFor("home"));
        }
    };
};

// Helper function (filter) to prevent non-loggedin users from access
$authenticated = function () use ($authenticationCheck) {
    return $authenticationCheck(true);
};

// Helper function (filter) to prevent loggedin users from access
$guest = function () use ($authenticationCheck) {
    return $authenticationCheck(false);
};
