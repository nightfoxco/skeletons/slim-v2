<?php

$app->get("/auth/activate", function () use ($app) {

    $request = $app->request;

    $email = $request->get("email");
    $identifier = $request->get("identifier");

    $hashedIdentifier = $app->hash->hash($identifier);

    $user = $app->user->where("email", $email)->where("active", false)->first();

    if (!$user || !$app->hash->hashCheck($user->active_hash, $hashedIdentifier)) {
        $app->flash("global", "There was a problem activating your account.");
        return $app->redirect($app->urlFor("auth.signin"));
    }

    $user->activateAccount();

    $app->flash("global", "Your Account has been successfully activated.");
    return $app->redirect($app->urlFor("auth.signin"));

})->name("auth.activate");
