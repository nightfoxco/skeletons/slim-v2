<?php

$app->get("/auth/signout", $authenticated(), function () use ($app) {

    // Unset Session variable that holds currently authenticated  user id
    unset($_SESSION[$app->config->get("auth.session")]);

    // Remove Remember Credentials from Cookie and Database
    if ($app->getCookie($app->config->get("auth.remember"))) {
        $app->auth->removeRememberCredentials();
        $app->deleteCookie($app->config->get("auth.remember"));
    }

    // Flash Message and redirect to home page
    $app->flash("global", "You have been successfully logged out");
    return $app->redirect($app->urlFor("home"));

})->name("auth.signout");
