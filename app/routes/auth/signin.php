<?php

use Respect\Validation\Validator as v;
use Carbon\Carbon;

$app->get("/auth/signin", $guest(), function () use ($app) {

    $app->render("auth/signin.twig");

})->name("auth.signin");

$app->post("/auth/signin", $guest(), function () use ($app) {

    $request = $app->request;

    $email = $request->post("email");
    $password = $request->post("password");

    $v = $app->validation;

    $v->validate($request, [
        "email" => v::notEmpty()->email(),
        "password" => v::notEmpty()
    ]);

    if ($v->passes()) {
        // Check for User in database
        $user = $app->user->where("email", $email)->first();

        // Redirect back with Flash Message if User does not exist or password is incorrect
        if (!$user || !$app->hash->passwordCheck($password, $user->password)) {
            $app->flash("global", "E-Mail Address and/or Password incorrect!");
            return $app->redirect($app->urlFor("auth.signin"));
        }

        // Set session variable to currently authenticated user id
        $_SESSION[$app->config->get("auth.session")] = $user->id;

        // Set Cookie to keep User signed in after session expires
        $rememberIdentifier = $app->randomlib->generateString(128);
        $rememberToken = $app->randomlib->generateString(128);

        $user->updateRememberCredentials(
            $rememberIdentifier,
            $app->hash->hash($rememberToken)
        );

        $app->setCookie(
            $app->config->get("auth.remember"),
            "{$rememberIdentifier}___{$rememberToken}",
            Carbon::parse("+4 week")->timestamp
        );

        // After successfully logging in, Redirect to Home page and Flash Message
        $app->flash("global", "Sie wurden erfolgreich eingeloggt.");
        return $app->redirect($app->urlFor("home"));
    }

    $app->render("auth/signin.twig", [
        "request" => $request,
        "errors" => $v->errors()
    ]);
});
