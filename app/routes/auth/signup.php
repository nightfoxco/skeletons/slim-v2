<?php

use Respect\Validation\Validator as v;

$app->get("/auth/signup", $guest(), function () use ($app) {

    $app->render("auth/signup.twig");

})->name("auth.signup");

$app->post("/auth/signup", $guest(), function () use ($app) {

    $request = $app->request;

    $email = $request->post("email");
    $password = $request->post("password");

    $v = $app->validation;

    $v->validate($request, [
        "email" => v::notEmpty()->length(5, 256)->email()->emailAvailable($app->user),
        "password" => v::length(8)
    ]);

    if ($v->passes()) {
        $identifier = $app->randomlib->generateString(128);

        $user = $app->user->create([
            "email" => $email,
            "password" => $app->hash->password($password),
            "active_hash" => $app->hash->hash($identifier)
        ]);

        // Set session variable to currently authenticated user id to sign user in
        $_SESSION[$app->config->get("auth.session")] = $user->id;

        $app->mail->send("mail/auth/signup.twig", ["user" => $user, "identifier" => $identifier], function ($message) use ($user) {
            $message->to($user->email);
            $message->subject("Confirm Account registration");
        });

        $app->flash("global", "You have been successfully registered.");
        return $app->redirect($app->urlFor("home"));
    }

    $app->render("auth/signup.twig", [
        "request" => $request,
        "errors" => $v->errors()
    ]);
});
