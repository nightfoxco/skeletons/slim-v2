<?php

$app->get("/", function () use ($app) {

    // Render home.twig file
    $app->render("home.twig");

})->name("home");
