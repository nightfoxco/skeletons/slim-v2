<?php

namespace App\Validation\Rules;

use Respect\Validation\Rules\AbstractRule;
use App\Models\User;

class EmailAvailable extends AbstractRule
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function validate($input)
    {
        $user = $this->user->where("email", $input);

        return $user->count() === 0;
    }
}
