<?php

// Start the Session
session_cache_limiter(false);
session_start();

// Require Slim Application
require_once dirname(__DIR__) . "/bootstrap/app.php";

// Run Slim Application
$app->run();
