let show_password_button = $("#show-password");
let hide_password_button = $("#hide-password");

show_password_button.on("click", function(event) {
    event.preventDefault();

    $("#password")[0].type = "text";

    show_password_button[0].style.display = "none";
    hide_password_button[0].style.display = "block";
});

hide_password_button.on("click", function(event) {
    event.preventDefault();

    $("#password")[0].type = "password";

    show_password_button[0].style.display = "block";
    hide_password_button[0].style.display = "none";
});