<?php

require_once __DIR__ . '/bootstrap/app.php';

return [
    'paths' => [
        'migrations' => 'database/migrations'
    ],

    'environments' => [
        'default_migration_table' => 'migrations',

        'default' => [
            'adapter' => $app->config->get('db.driver'),
            'host' => $app->config->get('db.host'),
            'port' => $app->config->get('db.port'),
            'name' => $app->config->get('db.database'),
            'user' => $app->config->get('db.username'),
            'pass' => $app->config->get('db.password'),
            'charset' => $app->config->get('db.charset'),
            'collation' => $app->config->get('db.collation'),
            'table_prefix' => $app->config->get('db.prefix'),
        ]
    ]
];
