<?php

return [
    "app" => [
        "url" => "https://example.com",
        "hash" => [
            "algo" => PASSWORD_BCRYPT,
            "cost" => 10
        ],
        "debug" => true
    ],

    "db" => [
        "driver" => "mysql",
        "host" => "127.0.0.1",
        "port" => 3306,
        "database" => "database",
        "username" => "root",
        "password" => "root",
        "charset" => "utf8",
        "collation" => "utf8_unicode_ci",
        "prefix" => ""
    ],

    "auth" => [
        "session" => "user_id",
        "remember" => "user_remember"
    ],

    "mail" => [
        "smtp_auth" => true,
        "smtp_secure" => "tls",
        "host" => "smtp.example.com",
        "name" => "name",
        "username" => "username",
        "password" => "password",
        "port" => 587,
        "html" => true
    ],

    "csrf" => [
        "key" => "csrf_token"
    ]
];
